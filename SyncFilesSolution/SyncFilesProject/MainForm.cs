﻿using System;
using System.Diagnostics;
using System.IO;
using System.Threading;
using System.Windows.Forms;
using Microsoft.Win32;

namespace SyncFilesProject
{
    public partial class MainForm : Form
    {
        private Process statusProcess;

        public MainForm()
        {
            InitializeComponent();
            SetStartup();
            InitPath();
            labelStatus.Text = "Waiting for device...";
            Thread statusThread = new Thread(new ThreadStart(MonitorDevice));
            statusThread.IsBackground = true;
            statusThread.Start();
        }

        private void SetStartup()
        {
            RegistryKey registryKey = Registry.CurrentUser.OpenSubKey(AppData.STARTUP_REGISTRY_KEY, true);
            if (registryKey.GetValue(AppData.APP_NAME) == null)
                registryKey.SetValue(AppData.APP_NAME, Application.ExecutablePath.ToString());
        }

        private void InitPath()
        {
            textBoxDestDir.Text = AppData.DEST_DIR;
            if (!Directory.Exists(AppData.DEST_DIR))
                errorProvider.SetError(textBoxDestDir, "Destination Directory does not exist");
        }

        private void MonitorDevice()
        {
            string command = "status-window";
            statusProcess = StartAdbProcess(command);
            string line;
            while ((line = statusProcess.StandardOutput.ReadLine()) != string.Empty)
                if (line.EndsWith("device")) // new device connected
                    TransferFile();
                else if (line.EndsWith("unknown") || line.EndsWith("offline")) // device disconnected
                {
                    labelStatus.Text = "Waiting for device...";
                    notifyIcon.ShowBalloonTip(AppData.BALLON_TIMEOUT, AppData.APP_NAME, "No device connected", ToolTipIcon.Info);
                    buttonRetry.Enabled = false;
                }
        }

        private void TransferFile()
        {
            string command = string.Format("pull {0} {1}", AppData.SRC_DIR, AppData.DEST_DIR);
            buttonBrowse.Enabled = false;
            Process adbProcess = StartAdbProcess(command);
            labelStatus.Text = "Transferring file...";
            adbProcess.WaitForExit();
            buttonBrowse.Enabled = true;
            if (adbProcess.ExitCode == 0)
            {
                string successMessage = "File transferred succesfully";
                notifyIcon.ShowBalloonTip(AppData.BALLON_TIMEOUT, AppData.APP_NAME, successMessage, ToolTipIcon.Info);
                labelStatus.Text = successMessage;
                buttonRetry.Enabled = false;
                DeleteFile();
            }
            else
            {
                string errorMessage = "Error while transferring file";
                notifyIcon.ShowBalloonTip(AppData.BALLON_TIMEOUT, AppData.APP_NAME, errorMessage, ToolTipIcon.Error);
                labelStatus.Text = errorMessage;
                buttonRetry.Enabled = true;
            }
        }

        private void DeleteFile()
        {
            string command;
            if (AppData.SRC_DIR.EndsWith("/"))
                command = string.Format("shell rm {0}*", AppData.SRC_DIR);
            else
                command = string.Format("shell rm {0}/*", AppData.SRC_DIR);
            StartAdbProcess(command);
        }

        private Process StartAdbProcess(string command)
        {
            Process adbProcess = new Process();
            adbProcess.StartInfo.FileName = AppData.ADB_PATH;
            adbProcess.StartInfo.Arguments = command;
            adbProcess.StartInfo.UseShellExecute = false;
            adbProcess.StartInfo.CreateNoWindow = true;
            adbProcess.StartInfo.WorkingDirectory = Application.StartupPath.ToString();
            adbProcess.StartInfo.RedirectStandardOutput = true;
            adbProcess.Start();
            return adbProcess;
        }

        private void ButtonBrowse_Click(object sender, EventArgs e)
        {
            DialogResult result = folderBrowserDialog.ShowDialog();
            if (result == DialogResult.OK)
            {
                string destDir = folderBrowserDialog.SelectedPath;
                AppData.DEST_DIR = destDir;
                textBoxDestDir.Text = destDir;
                errorProvider.Clear();
            }
        }

        private void MainForm_Resize(object sender, EventArgs e)
        {
            if (WindowState == FormWindowState.Minimized)
            {
                Hide();
                string message = string.Format("{0} is still running", AppData.APP_NAME);
                notifyIcon.ShowBalloonTip(AppData.BALLON_TIMEOUT, AppData.APP_NAME, message, ToolTipIcon.Info);
            }
        }

        private void NotifyIcon_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            Show();
            WindowState = FormWindowState.Normal;
            BringToFront();
        }

        private void ButtonRetry_Click(object sender, EventArgs e)
        {
            TransferFile();
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            statusProcess.Kill();
        }
    }
}
