﻿using System.Configuration;
using System.Windows.Forms;

namespace SyncFilesProject
{
    static class AppData
    {
        public static string ADB_PATH { get { return @"adb\adb.exe"; } }

        public static string STARTUP_REGISTRY_KEY { get { return @"SOFTWARE\Microsoft\Windows\CurrentVersion\Run"; } }

        public static string SRC_DIR { get { return ConfigurationManager.AppSettings["srcDir"]; } }

        public static string DEST_DIR
        {
            get
            {
                string destDir = ConfigurationManager.AppSettings["destDir"];
                if (destDir == ".")
                    destDir = Application.StartupPath;
                return destDir;
            }
            set
            {
                Configuration config = ConfigurationManager.OpenExeConfiguration(Application.ExecutablePath.ToString());
                config.AppSettings.Settings["destDir"].Value = value;
                config.Save();
            }
        }

        public static string APP_NAME { get { return "FileSync"; } }

        public static int BALLON_TIMEOUT { get { return 300; } }
    }
}
