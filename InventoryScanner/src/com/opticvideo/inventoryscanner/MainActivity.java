package com.opticvideo.inventoryscanner;

import java.io.File;
import java.io.IOException;
import java.util.Date;

import com.opticvideo.database.CSVWriter;
import com.opticvideo.database.DisplayUnitDBHelper;
import com.opticvideo.database.InventoryOpDBHelper;

import android.os.Bundle;
import android.os.Environment;
import android.app.Activity;
import android.database.Cursor;
import android.view.Menu;

public class MainActivity extends Activity {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		testDisplayUnit();
		testOperationInventory();
		//		

		setContentView(R.layout.activity_main);  
	}

	void testOperationInventory(){

		String path = "/sdcard/";//Environment.getExternalStorageDirectory().getAbsolutePath();
		InventoryOpDBHelper stock_helper = new InventoryOpDBHelper(this.getApplicationContext(), "specsaver_db", 1);

		stock_helper.deleteFromTables();

		stock_helper.insertProductCount(30, "sc40", new Date(), 1, 1);
		stock_helper.insertProductCount(36, "sc40", new Date(), 1, 1);
		stock_helper.insertProductCount(33, "sc40", new Date(), 1, 1);
		stock_helper.insertProductCount(20, "sc40", new Date(), 1, 1);
		stock_helper.insertProductCount(34, "sc40", new Date(), 1, 1);
		stock_helper.insertProductCount(31, "sc40", new Date(), 1, 1);
		stock_helper.insertProductCount(39, "sc40", new Date(), 1, 1);

		stock_helper.insertOperation("sc50", new Date());
		stock_helper.insertOperation("sc50", new Date());
		stock_helper.insertOperation("sc51", new Date());
		stock_helper.insertOperation("sc52", new Date());
		stock_helper.insertOperation("sc53", new Date());

		int unfinised = stock_helper.getUnfinishedOperation();
		System.out.println("unfinised stock counts "+unfinised);

		//stock_helper.updateOperation("sc50", null);
		stock_helper.updateOperation("sc100", new Date());

		unfinised = stock_helper.getUnfinishedOperation();
		System.out.println("unfinised stock counts "+unfinised);

		stock_helper.updateOperation("sc52", null);
		stock_helper.updateOperation("sc53", null);

		unfinised = stock_helper.getUnfinishedOperation();
		System.out.println("unfinised stock counts "+unfinised);
		
		int status = stock_helper.getOperationStatus("sc53");
		System.out.println("status for "+"sc53: "+status);
		
		status = stock_helper.getOperationStatus("sc24000");
		System.out.println("status for "+"sc240000: "+status);
		
		status = stock_helper.getOperationStatus("sc50");
		System.out.println("status for "+"sc50: "+status);
		

		Cursor cursor = stock_helper.getProducts("sc40", null);
		File file = new File(path,"all.csv");
		try {
			CSVWriter.exportToCSV(cursor, file);
		} catch (IOException e) {
			System.out.println("Error while exporting file to csv");;
			e.printStackTrace();
		}

		Cursor cursor1 = stock_helper.getProducts("sc40", new String[]{InventoryOpDBHelper.LOC_ID_C,InventoryOpDBHelper.SKU_C});
		File file1 = new File(path,"someCol.csv");
		try {
			CSVWriter.exportToCSV(cursor1, file1);
		} catch (IOException e) {
			System.out.println("Error while exporting file to csv");;
			e.printStackTrace();
		}

		Cursor cursor2 = stock_helper.getProducts("sc200", null);
		File file2 = new File(path,"dontExist.csv");
		try {
			CSVWriter.exportToCSV(cursor2, file2);
		} catch (IOException e) {
			System.out.println("Error while exporting file to csv");;
			e.printStackTrace();
		}

		stock_helper.deleteFromTables();

		Cursor cursor3 = stock_helper.getProducts("sc200", null);
		File file3 = new File(path,"deleted.csv");
		try {
			CSVWriter.exportToCSV(cursor3, file3);
		} catch (IOException e) {
			System.out.println("Error while exporting file to csv");;
			e.printStackTrace();
		}


	}

	void testDisplayUnit(){

		String path = "/sdcard/"; //Environment.getExternalStorageDirectory().getAbsolutePath();
		DisplayUnitDBHelper displayUnit = new DisplayUnitDBHelper(getApplicationContext(), "display_unit.db", 1);
		displayUnit.deleteFromTables();

		displayUnit.insertLocation(1, "locatia 1", 100, 100);
		displayUnit.insertLocation(1, "locatia 1", 100, 100);
		displayUnit.insertLocation(1, "locatia 1", 100, 100);
		displayUnit.insertLocation(2, "locatia 2", 100, 100);
		displayUnit.insertLocation(3, "locatia 3", 100, 100);
		displayUnit.insertLocation(4, "locatia 4", 100, 100);
		displayUnit.insertLocation(5, "locatia 5", 100, 100);
		int unfinishedLoc = displayUnit.getUnfishedLocationScans();
		System.out.println("unfinished locations :"+unfinishedLoc);
		
		int status = displayUnit.getScanLocationStatus(1);
		System.out.println("status for :"+1+" "+status);
		
		displayUnit.updateLocation(1);
		displayUnit.updateLocation(2);
		displayUnit.updateLocation(3);
		
		status = displayUnit.getScanLocationStatus(1);
		System.out.println("status for :"+1+" "+status);
		
		status = displayUnit.getScanLocationStatus(7000);
		System.out.println("status for :"+7000+" "+status);
		
		unfinishedLoc = displayUnit.getUnfishedLocationScans();
		System.out.println("unfinished locations :"+unfinishedLoc);
		displayUnit.updateLocation(1);
		unfinishedLoc = displayUnit.getUnfishedLocationScans();
		System.out.println("unfinished locations :"+unfinishedLoc);

		unfinishedLoc = displayUnit.getUnfishedLocationScans();
		System.out.println("unfinished locations :"+unfinishedLoc);


		displayUnit.insertProductLocation("sku1", 1, 1, 1, new Date());
		displayUnit.insertProductLocation("sku2", 1, 1, 1, new Date());
		displayUnit.insertProductLocation("sku3", 1, 1, 1, new Date());
		displayUnit.insertProductLocation("sku2", 1, 1, 2, new Date());
		displayUnit.insertProductLocation("sku1", 1, 1, 3, new Date());
		displayUnit.insertProductLocation("sku3", 1, 1, 4, new Date());
		displayUnit.insertProductLocation("sku4", 1, 1, 5, new Date());


		Cursor data = displayUnit.getProducts(1, null);
		File file = new File(path, "allColumns.csv");
		try{
			CSVWriter.exportToCSV(data, file);
		}catch (IOException e) {
			System.out.println("Error while exporting file to csv!!!");;
			e.printStackTrace();
		}

		Cursor data1 = displayUnit.getProducts(1000, null);
		File file1 = new File(path, "noInfo.csv");
		try{
			CSVWriter.exportToCSV(data1, file1);
		}catch (IOException e) {
			System.out.println("Error while exporting file to csv!!!");;
			e.printStackTrace();
		}

		Cursor data2 = displayUnit.getProducts(1, new String[]{DisplayUnitDBHelper.SKU_C, DisplayUnitDBHelper.LOC_ID_C, DisplayUnitDBHelper.ROW_C, DisplayUnitDBHelper.COL_C});
		File file2 = new File(path, "someColumns.csv");
		try{
			CSVWriter.exportToCSV(data2, file2);
		}catch (IOException e) {
			System.out.println("Error while exporting file to csv!!!");;
			e.printStackTrace();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}
}
