package com.opticvideo.database;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import android.database.Cursor;
import android.util.Log;

public class CSVWriter {

	public static final String TAG = CSVWriter.class.getSimpleName();
	/** Default separator  */
	public static final char DEFAULT_SEPARATOR = ',';
	/** Default new line*/
	public static final char DEFAPULT_NEW_LINE = '\n';

	public static boolean exportToCSV (Cursor data, File file) throws IOException{

		if(data == null) {
			Log.i(TAG, "Empty cursor in exportToCSV method");
			return false;
		}
		FileWriter fw = new FileWriter(file.getAbsoluteFile());
		BufferedWriter bw = new BufferedWriter(fw);
		
		if(!data.moveToFirst()) {
			data.close();
			bw.close();
			return true;
		}
		
		do{
			int columnCount = data.getColumnCount();
			for(int i = 0; i < columnCount; i++){
				bw.write(data.getString(i));
				if(i!= columnCount-1)
					bw.write(DEFAULT_SEPARATOR);
			}
			bw.newLine();
		}while(data.moveToNext());
		
		//close the cursor
		data.close();

		bw.close();
		return true;
	}

}
