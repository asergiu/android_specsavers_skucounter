package com.opticvideo.database;

import java.util.Date;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DisplayUnitDBHelper extends SQLiteOpenHelper{

	private static final String TAG = DisplayUnitDBHelper.class.getSimpleName();
	//TODO move ??
	public static final String SCAN_LOC_T = "ScanLocation";
	public static final String LOC_ID_C = "location_id";
	public static final String LOC_NAME_C = "location_name";
	public static final String MAX_COLS_C = "no_cols";
	public static final String MAX_ROWS_C = "no_rows";
	public static final String FINALIZED_C = "finalized";

	public static final String PROD_LOC_T = "ProductLocation";
	public static final String SKU_C = "sku";
	public static final String ROW_C = "row";
	public static final String COL_C = "col";
	public static final String SCANNED_AT = "scanned_at";

	public DisplayUnitDBHelper(Context context, String name, int version) {

		super(context, name, null, version);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// create database tables
		String createLocTable = String.format("CREATE TABLE %s(%s INTEGER PRIMARY KEY, %s TEXT, %s INT, %s INT, %s INT)", SCAN_LOC_T, LOC_ID_C, LOC_NAME_C, MAX_ROWS_C, MAX_COLS_C, FINALIZED_C);
		Log.d(TAG, createLocTable);

		String createProdLocTable = String.format("CREATE TABLE %s(%s TEXT, %s INTEGER, %s INT, %s INT, %s NUMERIC, PRIMARY KEY (%s, %s, %s))",
				PROD_LOC_T, SKU_C, LOC_ID_C, ROW_C, COL_C, SCANNED_AT, ROW_C, COL_C, LOC_ID_C);
		Log.d(TAG, createProdLocTable);

		try{
			db.execSQL(createLocTable);
			db.execSQL(createProdLocTable);
		}
		catch(SQLException ex){
			Log.e(TAG, "Error creating tables! Exception occured!!");
		}

	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub

	}

	public boolean insertLocation(int locId, String locName, int maxRows, int maxCols){

		boolean ok = true;
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put(LOC_ID_C, locId);
		values.put(LOC_NAME_C, locName); 
		values.put(MAX_COLS_C, maxCols);
		values.put(MAX_ROWS_C, maxRows);
		values.put(FINALIZED_C, 0);

		try{
			long result = db.insertOrThrow(SCAN_LOC_T, null, values);
			Log.i(TAG, "Inserted row "+result+" in "+SCAN_LOC_T);
		}catch(SQLException ex){
			Log.e(TAG, "Error while inserting row in " +SCAN_LOC_T+" table! Exception occured!!!");
			ok = false;
		}
		return ok;
	}


	public boolean insertProductLocation(String sku, int locId, int row, int col, Date scannedAt){

		boolean ok = true;
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put(SKU_C, sku);
		values.put(LOC_ID_C, locId);
		values.put(ROW_C, row);
		values.put(COL_C, col);
		values.put(SCANNED_AT, scannedAt.getTime());

		try{
			long result =db.insertOrThrow(PROD_LOC_T, null, values);
			Log.i(TAG, "Inserted row "+result+" in "+PROD_LOC_T+ " table!");
		}catch(SQLException ex){
			Log.e(TAG, "Error while inserting row in " +PROD_LOC_T+" table! Exception occured!!!");
			ok = false;
		}
		return ok;
	}

	public void updateLocation(int locId){

		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(FINALIZED_C, 1);

		String whereClause = LOC_ID_C+"=?";
		int result = db.update(SCAN_LOC_T, values, whereClause, new String[]{String.valueOf(locId)});

		Log.i(TAG, "Updated "+result+ " rows in "+SCAN_LOC_T+" table!");
	}

	public int getUnfishedLocationScans(){

		SQLiteDatabase db = this.getReadableDatabase();

		Cursor cursor = db.query(SCAN_LOC_T, null, FINALIZED_C + "=?", new String[]{String.valueOf(0)}, null, null, null, null);

		if(cursor == null)
			return -1;

		int count = cursor.getCount();
		cursor.close();
		
		return count;
	}

	public Cursor getProducts(int locationId, String[] columns){

		SQLiteDatabase db = this.getReadableDatabase();

		Cursor cursor = db.query(PROD_LOC_T, columns, LOC_ID_C + "=?", new String[]{String.valueOf(locationId)}, null, null, null, null);

		if(cursor != null){
			cursor.moveToFirst();
		}else
			Log.i(TAG, "No entries found with "+LOC_ID_C+ " = "+locationId);

		return cursor;
	}
	
	public void deleteFromTables(){
		
		SQLiteDatabase db = this.getWritableDatabase();
		db.delete(SCAN_LOC_T, null, null);
		db.delete(PROD_LOC_T, null, null);
	}
	
	
	public int getScanLocationStatus(int locationId){
		
		int status = 0;
		SQLiteDatabase db = this.getReadableDatabase();
		
		Cursor cursor = db.query(SCAN_LOC_T, new String[]{FINALIZED_C}, LOC_ID_C+"=?",  new String[]{String.valueOf(locationId)}, null,null, null, null);
		
		if(cursor==null || cursor.getCount()==0){
			cursor.close();
			return -1;
		}
		
		cursor.moveToFirst();
		status = cursor.getInt(0);
		cursor.close();
		
		return status;
	}
	

}
