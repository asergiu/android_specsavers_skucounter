package com.opticvideo.database;

import java.util.Date;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class InventoryOpDBHelper extends SQLiteOpenHelper{

	private final static String TAG =  InventoryOpDBHelper.class.getSimpleName();
	public final static String DB_NAME = "stock_counting.db";

	// ProductInventory table contains information about the products on an inventory operation
	public final static String PROD_INVENTORY_T = "ProductInventory";
	// product SKU
	public final static String SKU_C = "sku";
	// ?? is this necessary
	public final static String SCANNED_AT_C = "scanned_at";
	// product count
	public final static String COUNT_C = "count";
	// location id
	public final static String LOC_ID_C = "location_id";

	// StockCount table contains information about the inventory operation
	public final static String INVENTORY_OPERATION_T = "InventoryOperation";
	// stock count id
	public final static String OPERATION_ID_C = "operation_id";
	// start date of stock count
	public final static String START_DATE_C = "start_date";
	// end date of stock count
	public final static String END_DATE_C = "end_date";
	// operation status - 0 not finished, 1 finished
	public final static String FINALIZED_C = "finalized";

	public InventoryOpDBHelper(Context context, String name, int version) {
		super(context, DB_NAME, null, version);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {

		//Create tables
		String createStockCountTable = String.format("CREATE TABLE %s (%s TEXT PRIMARY KEY, %s INTEGER, %s INTEGER, %s INTEGER)", INVENTORY_OPERATION_T,
				OPERATION_ID_C, START_DATE_C, END_DATE_C, FINALIZED_C);

		String createProductStockTable = String.format("CREATE TABLE %s (%s NUMERIC, %s TEXT, %s NUMERIC, %s INTEGER, %s INTEGER, PRIMARY KEY (%s, %s) )", 
				PROD_INVENTORY_T, SKU_C, OPERATION_ID_C, SCANNED_AT_C, COUNT_C, LOC_ID_C, SKU_C, OPERATION_ID_C);
		System.out.println(createProductStockTable);

		db.execSQL(createStockCountTable);
		db.execSQL(createProductStockTable);

		// enable foreign key constraints
		//db.execSQL(" PRAGMA foreign_keys = ON");

	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

		// Drop older table if existed
		db.execSQL("DROP TABLE IF EXISTS " + PROD_INVENTORY_T);
		db.execSQL("DROP TABLE IF EXISTS " + INVENTORY_OPERATION_T);

		// Create tables again
		onCreate(db);
	}

	public boolean insertOperation(String stockCountId, Date startDate){

		boolean ok = true;
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(OPERATION_ID_C, stockCountId);
		// store the number of seconds since January 1st 1970
		values.put(START_DATE_C, String.valueOf(startDate.getTime())); 
		values.put(FINALIZED_C, String.valueOf(0));

		try{
			long result = db.insertOrThrow(INVENTORY_OPERATION_T, null, values);
			Log.i(TAG, "Inserted row "+result+" in "+INVENTORY_OPERATION_T);

		}catch(SQLException ex){
			Log.e(TAG, "Error while inserting row in StockCount table! Exception occured!!!");
			ok = false;
		}

		return ok;
	}

	/// set operation status to finalized
	public void updateOperation(String stockCountId, Date endDate){

		SQLiteDatabase db = this.getWritableDatabase();

		// if no end date provided set current time as date
		if(endDate == null)
			endDate = new Date();

		ContentValues values = new ContentValues();
		// store the number of seconds since January 1st 1970
		values.put(END_DATE_C, String.valueOf(endDate.getTime())); 
		values.put(FINALIZED_C, 1);

		String whereClause = OPERATION_ID_C+"=?";
		int result = db.update(INVENTORY_OPERATION_T, values, whereClause, new String[]{String.valueOf(stockCountId)});

		Log.d(TAG, "Updated rows in StockCount table "+result);

	}

	public void deleteFromTables(){

		SQLiteDatabase db = this.getReadableDatabase();
		db.delete(PROD_INVENTORY_T, null, null);
		db.delete(INVENTORY_OPERATION_T, null, null);
	}

	public boolean insertProductCount(long sku, String stockCountId, Date scannedAt, int productCount, int locationId){

		boolean ok = true;
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put(SKU_C, String.valueOf(sku));
		values.put(OPERATION_ID_C, stockCountId);
		values.put(SCANNED_AT_C, String.valueOf(scannedAt.getTime()));
		values.put(COUNT_C, productCount);
		values.put(LOC_ID_C, locationId);

		try{
			long result = db.insertOrThrow(PROD_INVENTORY_T, null, values);
			Log.i(TAG, "Inserted row "+result+" in "+PROD_INVENTORY_T);
		}catch(SQLException ex){
			Log.e(TAG, "Error while inserting row in StockCount table");
			ok = false;
		}

		return ok;
	}

	public int getUnfinishedOperation(){

		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.query(INVENTORY_OPERATION_T, null, FINALIZED_C + "=?", new String[]{String.valueOf(0)}, null, null, null, null);
		int count;

		if(cursor == null) 
			count = -1;
		else{
			count = cursor.getCount();
		}

		cursor.close();
		return count;
	}

	public Cursor getProducts(){

		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = null;

		String sql = "SELECT * FROM" + PROD_INVENTORY_T;
		cursor = db.rawQuery(sql, null);
		if(cursor != null){
			System.err.println(cursor.getCount());
			cursor.moveToFirst();
		}
		return cursor;
	}

	public Cursor getProducts(String stockCountId, String[] columns){

		SQLiteDatabase db = this.getReadableDatabase();

		Cursor cursor = db.query(PROD_INVENTORY_T, columns, OPERATION_ID_C + "=?", new String[]{stockCountId}, null, null, null, null);
		if(cursor == null){
			Log.i(TAG, "Error occured in query for products with "+OPERATION_ID_C+" = "+stockCountId);
		}
		return cursor;
	}

	public int getOperationStatus(String operationId){

		int status = 0;
		SQLiteDatabase db = this.getReadableDatabase();

		Cursor cursor = db.query(INVENTORY_OPERATION_T, new String[]{FINALIZED_C}, OPERATION_ID_C+"=?",  new String[]{String.valueOf(operationId)}, null,null, null, null);

		if(cursor==null || cursor.getCount()==0){
			cursor.close();
			return -1;
		}

		cursor.moveToFirst();
		status = cursor.getInt(0);
		cursor.close();

		return status;
	}

}